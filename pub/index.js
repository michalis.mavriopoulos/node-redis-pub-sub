// const json = require("milliparsec");
const Redis = require("ioredis");
const express = require("express");

const redis = new Redis();

const app = express();
// app.use(json());

app.post("/publish", (req, res) => {
  //   redis.publish("send-user-data", JSON.stringify({ ts: new Date() }));
  redis.xadd("mystream", "*", "id", new Date());
  return res.json({ msg: "I hope this runs 😅" });
});

app.listen(4000, () => {
  try {
    // the group might have been already created
    // redis cmd: XGROUP CREATE newstream mygroup $ MKSTREAM
    redis.xgroup("CREATE", "mystream", "mygroup", "$", "MKSTREAM");
  } catch (err) {
    console.log(err);
  }

  console.log("server started");
});

process.on("uncaughtException", (error) => {
  console.error(error);
});
